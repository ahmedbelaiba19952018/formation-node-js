const souscategoryModel = require('../models/subcategory.model');
module.exports = {
    create: function (req, res, next) {
        souscategoryModel.create({
            name: req.body.name,
            description: req.body.description,
            category: req.body.category
        }, function (err, result) {
            if (err)
                next(err);
            else
                res.json({status: "success", message: "sous category added successfully!!!", data: null});

        });
    },
    getAll: function (req, res, next) {

        souscategoryModel.find({},"name description").populate("category","name description").exec(function (err, subcategorys) {
            if (err) {
                next(err);
            } else {

                res.json({status: "success", message: "subcategorys list found!!!", data: {order: subcategorys}});

            }
        });
    }

}