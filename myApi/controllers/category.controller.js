const categoryModel = require('../models/category.model');
module.exports = {
    create: function (req, res, next) {
    categoryModel.create({
        name: req.body.name,
        description: req.body.description,
        listsubcategory: req.body.listsubcategory
    }, function (err, result) {
        if (err)
            next(err);
        else
            res.json({status: "success", message: "category added successfully!!!", data: null});

    });
}
}