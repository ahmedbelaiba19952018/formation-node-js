// l'imporation du model
const productModel = require('../models/product.model');
module.exports = {

    /*********************************************/
    getById: function (req, res, next) {
        console.log(req.body);
        productModel.findById(req.params.productId, function (err, productInfo) {
            if (err) {
                next(err);
            } else {
                res.json({status: "success", message: "Product found!!!", data: {product: productInfo}});
            }
        });
    },
    /***********************************************************/
    getAll: function (req, res, next) {
        var productList = [];
        productModel.find({}, function (err, products) {
            if (err) {
                next(err);
            } else {
                for (var product of products) {
                    productList.push({
                        id: product._id,
                        name: product.name,
                        prix: product.prix,
                        Qte: product.Qte,
                        description: product.description
                    });
                }
                res.json({status: "success", message: "Product list found!!!", data: {product: productList}});

            }
        });
    },
    /***************************************************/
    updateById: function (req, res, next) {
        productModel.findByIdAndUpdate(req.params.productId, {name: req.body.name}, function (err, productInfo) {
            if (err)
                next(err);
            else {
                res.json({status: "success", message: "Product updated successfully!!!", data: null});
            }
        });
    },
    deleteById: function (req, res, next) {
        productModel.findByIdAndRemove(req.params.productId, function (err, productInfo) {
            if (err)
                next(err);
            else {
                res.json({status: "success", message: "Product deleted successfully!!!", data: null});
            }
        });
    },
    create: function (req, res, next) {
        productModel.create({
            name: req.body.name, prix: req.body.prix, Qte: req.body.Qte,
            description: req.body.description
        }, function (err, result) {
            if (err)
                next(err);
            else
                res.json({status: "success", message: "Produit added successfully!!!", data: null});

        });
    },
}