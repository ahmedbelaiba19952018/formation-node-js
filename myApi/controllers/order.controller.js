// l'imporation du model
const orderModel = require('../models/order.model');
module.exports = {

    /*********************************************/
    getById: function (req, res, next) {
        console.log(req.body);
        orderModel.findById(req.params.orderId, function (err, orderInfo) {
            if (err) {
                next(err);
            } else {
                res.json({status: "success", message: "order found!!!", data: {order: orderInfo}});
            }
        });
    },
    /***********************************************************/
    getAll: function (req, res, next) {
        var orderList = [];
        orderList.find({}, function (err, orders) {
            if (err) {
                next(err);
            } else {
                for (var order of orders) {
                    orderList.push({
                        id: order._id,
                        name: order.name,
                        description: order.description
                    });
                }
                res.json({status: "success", message: "order list found!!!", data: {order: orderList}});

            }
        });
    },
    /***************************************************/
    updateById: function (req, res, next) {
        orderModel.findByIdAndUpdate(req.params.orderId, {name: req.body.name}, function (err, orderInfo) {
            if (err)
                next(err);
            else {
                res.json({status: "success", message: "order updated successfully!!!", data: null});
            }
        });
    },
    deleteById: function (req, res, next) {
        orderModel.findByIdAndRemove(req.params.orderId, function (err, orderInfo) {
            if (err)
                next(err);
            else {
                res.json({status: "success", message: "order deleted successfully!!!", data: null});
            }
        });
    },
    create: function (req, res, next) {
        orderModel.create({
            name: req.body.name,
            description: req.body.description
        }, function (err, result) {
            if (err)
                next(err);
            else
                res.json({status: "success", message: "order added successfully!!!", data: null});

        });
    }
}