const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const movieController = require('../controllers/movies.controller');

router.post('/', movieController.create);
router.delete('/:movieId', movieController.deleteById);
router.put('/:movieId', movieController.updateById);
router.get('/:movieId', movieController.getById);
router.get('/',validateUser, movieController.getAll);

module.exports = router;

function validateUser(req, res, next) {

    jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
        if (err) {
            res.json({status:"error", message: err.message, data:null});
        }else{
            // add user id to request
            req.body.userId = decoded.id;
            next();
        }
    });

}