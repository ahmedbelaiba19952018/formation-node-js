const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const orderController = require('../controllers/order.controller');

router.post('/', orderController.create);
router.delete('/:orderId', orderController.deleteById);
router.put('/:orderId', orderController.updateById);
router.get('/:orderId', orderController.getById);
router.get('/', orderController.getAll);

module.exports = router;
