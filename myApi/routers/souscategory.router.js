const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const souscategoryController = require('../controllers/souscategory.controller');

router.post('/', souscategoryController.create);
/*router.delete('/:orderId', souscategoryController.deleteById);
router.put('/:orderId', souscategoryController.updateById);
router.get('/:orderId', souscategoryController.getById);*/
router.get('/', souscategoryController.getAll);

module.exports = router;