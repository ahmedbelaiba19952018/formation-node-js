const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const productController = require('../controllers/product.controller');

router.post('/', productController.create);
router.delete('/:productId', productController.deleteById);
router.put('/:productId', productController.updateById);
router.get('/:productId', productController.getById);
router.get('/', productController.getAll);

module.exports = router;

