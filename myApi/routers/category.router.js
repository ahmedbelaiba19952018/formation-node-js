const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const categoryController = require('../controllers/category.controller');

router.post('/', categoryController.create);
/*router.delete('/:orderId', categoryController.deleteById);
router.put('/:orderId', categoryController.updateById);
router.get('/:orderId', categoryController.getById);
router.get('/', categoryController.getAll);*/

module.exports = router;