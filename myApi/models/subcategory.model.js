const mongoose = require('mongoose');
//Define a schema
const Schema = mongoose.Schema;
const subcategorySchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: true,
    },
    description: {
        type: String,
        trim: true,
        required: true,
    },
    category: {type: mongoose.Schema.Types.ObjectID, ref: "Category"}
});
module.exports = mongoose.model('Subcategory', subcategorySchema)
