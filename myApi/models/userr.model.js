const mongoose = require('mongoose');
//Define a schema
const Schema = mongoose.Schema;
const UserSchema = new Schema({
    name: {
        firstname: String,
        trim: true,
        required: true,
    },
    lastname: {
        type: String,
        trim: true,
        required: true
    },
    phone: {
        name: String,
        trim: true,
        required: true,
    }
});
module.exports = mongoose.model('User', UserSchema)
