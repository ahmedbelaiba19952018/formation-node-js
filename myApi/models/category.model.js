const mongoose = require('mongoose');
//Define a schema
const Schema = mongoose.Schema;
const CategorySchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    description: {
       type: String,
        trim: true,
        required: true
    },
    listsubcategory:[ {type: mongoose.Schema.Types.ObjectID, ref: "Subcategory"}]
});
module.exports = mongoose.model('Category', CategorySchema)
